package taskThreeCar;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

class CarTest {

    List<Car> cars = new ArrayList<>();

    @BeforeEach
    public void setUp(){
        cars.add(new Car("VW", "Polo", 2019, 100));
        cars.add(new Car("VW", "Golf", 2018, 1000000));
        cars.add(new Car("VW", "Passat", 1999, 299000));
        cars.add(new Car("VW", "Arteon", 2010, 666780));
        cars.add(new Car("VW", "Up!", 2000, 1222223));
        cars.add(new Car("Mercedes", "Cla", 1980, 540000));
        cars.add(new Car("Mercedes", "Gla", 1995, 1000000));
        cars.add(new Car("Mercedes", "Klasa A", 1999, 299000));
        cars.add(new Car("Mercedes", "Maybach", 2002, 666780));
        cars.add(new Car("Mercedes", "Vito", 2000, 1222223));
        cars.add(new Car("Audi", "A1", 1960, 540000));
        cars.add(new Car("Audi", "A3", 2010, 1000000));
        cars.add(new Car("Audi", "RS 3", 1999, 299000));
        cars.add(new Car("Audi", "Q2", 2002, 666780));
        cars.add(new Car("Audi", "E-tron", 2000, 122222300));
    }


    @Test
    public void shouldReturnSortedCarsByBrandWithComparator(){
        // given & when
        Collections.sort(cars, Car.getBrandComparator());
        // then
        Assertions.assertThat(cars.get(0).getBrand()).isEqualTo("Audi");
        Assertions.assertThat(cars.get(14).getBrand()).isEqualTo("VW");

    }

    @Test
    public void shouldReturnSortedCarsByBrandWithCompareToMethod(){
        // given & when
        Collections.sort(cars);
        // then
        Assertions.assertThat(cars.get(0).getBrand()).isEqualTo("Audi");
        Assertions.assertThat(cars.get(14).getBrand()).isEqualTo("VW");

    }

    @Test
    public void shouldReturnSortedCarsByModel(){

        // given & when
        Collections.sort(cars, Car.getModelComparator());

        // then
        Assertions.assertThat(cars.get(0).getModel()).isEqualTo("A1");
        Assertions.assertThat(cars.get(14).getModel()).isEqualTo("Vito");
    }

    @Test
    public void shouldReturnSortedCarsByYearsOfProduction(){

        // given & when
        Collections.sort(cars, Car.getYearOfProductionComparator());

        // then
        Assertions.assertThat(cars.get(0).getYearOfProduction()).isEqualTo(1960);
        Assertions.assertThat(cars.get(14).getYearOfProduction()).isEqualTo(2019);
    }

    @Test
    public void shouldReturnSortedCarsByAmountDrivenKM(){

        // given & when
        Collections.sort(cars, Car.getAmountOfDrivenKmComparator());

        // then
        Assertions.assertThat(cars.get(0).getAmountOfDrivenKm()).isEqualTo(100);
        Assertions.assertThat(cars.get(14).getAmountOfDrivenKm()).isEqualTo(122222300);
    }


    @Test
    public void shouldReturnSortedCarsByBrandAndModel(){
        // given & then
        Collections.sort(cars, Car.getBrandAndModelComparator());

        //then
        Assertions.assertThat(cars.get(0).getModel()).isEqualTo("A1");
        Assertions.assertThat(cars.get(4).getModel()).isEqualTo("RS 3");
        Assertions.assertThat(cars.get(5).getModel()).isEqualTo("Cla");
        Assertions.assertThat(cars.get(9).getModel()).isEqualTo("Vito");
        Assertions.assertThat(cars.get(10).getModel()).isEqualTo("Arteon");
        Assertions.assertThat(cars.get(14).getModel()).isEqualTo("Up!");

    }




}