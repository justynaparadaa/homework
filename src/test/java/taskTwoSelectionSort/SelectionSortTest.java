package taskTwoSelectionSort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SelectionSortTest {


    private SortAlgorithm sortAlgorithm = new SelectionSort();

    @Test
    public void shouldSortArray() {
        //given
        int[] tab = {6, 3, 8, 1, 0, 2};
        //when
        sortAlgorithm.sortSelection(tab);
        //then
        assertThat(tab).containsExactly(0, 1, 2, 3, 6, 8);
    }

    @Test
    public void shouldReturnAlreadySortedArray() {
        //given
        int[] tab = {1, 2, 3, 4, 5, 6};
        //when
        sortAlgorithm.sortSelection(tab);
        //then
        assertThat(tab).containsExactly(1, 2, 3, 4, 5, 6);
    }


    @Test
    void shouldFindIndexOfMaxElementInUnsortedTable() {
        // given
        int[] tab = {6, 3, 8, 1, 0, 2};
        // when
        int indexOfMaxElement = sortAlgorithm.findIndexOfMaxElement(tab);
        //then
        assertThat(indexOfMaxElement).isEqualTo(2);
    }

    @Test
    void shouldFindIndexOfMaxElementInSortedTable() {
        //given
        int[] tab = {1, 2, 3, 4, 5, 6};
        //when
        int indexOfMaxElement = sortAlgorithm.findIndexOfMaxElement(tab);
        //then
        assertThat(indexOfMaxElement).isEqualTo(5);
    }

    void shouldFindIndexOfMixElementInUnsortedTable() {
        // given
        int[] tab = {6, 3, 8, 1, 0, 2};
        // when
        int indexOfMinElement = sortAlgorithm.findIndexOfMinElement(tab);
        //then
        assertThat(indexOfMinElement).isEqualTo(4);
    }

    @Test
    void shouldFindIndexOfMixElementInSortedTable() {
        //given
        int[] tab = {1, 2, 3, 4, 5, 6};
        //when
        int indexOfMinElement = sortAlgorithm.findIndexOfMinElement(tab);
        //then
        assertThat(indexOfMinElement).isEqualTo(0);
    }
}