package taskTwoSelectionSort;

import javax.print.DocFlavor;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.lang.ClassCastException;

public class SelectionSort implements SortAlgorithm {

    public static void main(String[] args) {

        int[] arr = {3, 4, 5, 0, 7, 8, -10};

        SelectionSort object = new SelectionSort();

        object.sortSelection(arr);
        object.findIndexOfMinElement(arr);
        object.findIndexOfMaxElement(arr);

    }


    public void sortSelection(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int temp;
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            System.out.println(arr[i]);
        }
    }

    public int findIndexOfMaxElement(int[] arr) {
        int maxElement = arr[0];
        int maxIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > maxElement) {
                maxElement = arr[i];
                maxIndex = i;
            }
        }
        System.out.println("MaxIndx: " + maxIndex + " MaxElement: " + maxElement);
        return maxIndex;
    }

    public int findIndexOfMinElement(int[] arr) {
        int minElement = arr[0];
        int minIndx = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < minElement) {
                minElement = arr[i];
                minIndx = i;
            }
        }
        System.out.println("MinIndx: " + minIndx + " MinElement: " + minElement);
        return minIndx;
    }
}