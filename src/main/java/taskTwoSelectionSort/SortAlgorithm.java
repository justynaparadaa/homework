package taskTwoSelectionSort;

import java.util.List;

public interface SortAlgorithm {

    void sortSelection(int[] arr);
    int findIndexOfMaxElement(int[] arr);
    int findIndexOfMinElement(int[] arr);
}
