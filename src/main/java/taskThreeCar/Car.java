package taskThreeCar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Car implements Comparable<Car> {

    public static void main(String[] args) {

        List<Car> cars = new ArrayList<>();

        cars.add(new Car("VW", "Polo", 2019, 100));
        cars.add(new Car("VW", "Golf", 2018, 1000000));
        cars.add(new Car("VW", "Passat", 1999, 299000));
        cars.add(new Car("VW", "Arteon", 2010, 666780));
        cars.add(new Car("VW", "Up!", 2000, 1222223));
        cars.add(new Car("Mercedes", "Cla", 1980, 540000));
        cars.add(new Car("Mercedes", "Gla", 1995, 1000000));
        cars.add(new Car("Mercedes", "Klasa A", 1999, 299000));
        cars.add(new Car("Mercedes", "Maybach", 2002, 666780));
        cars.add(new Car("Mercedes", "Vito", 2000, 1222223));
        cars.add(new Car("Audi", "A1", 1960, 540000));
        cars.add(new Car("Audi", "A3", 2010, 1000000));
        cars.add(new Car("Audi", "RS 3", 1999, 299000));
        cars.add(new Car("Audi", "Q2", 2002, 666780));
        cars.add(new Car("Audi", "E-tron", 2000, 122222300));

//        cars.sort(getModelComparator());
//        System.out.println(cars.toString());
//        cars.sort(getYearOfProductionComparator());
//        System.out.println(cars.toString());


//        cars.sort(getAmountOfDrivenKmComparator());
//
//        for(taskThreeCar.Car taskThreeCar: cars) {
//            System.out.println(taskThreeCar.toString());
//        }

        cars.sort(getBrandAndModelComparator());

        for(Car car: cars){
            System.out.println(car.toString());
        }

    }

    private String brand;
    private String model;
    private int yearOfProduction;
    private int amountOfDrivenKm;


    public Car(String brand, String model, int yearOfProduction, int amountOfDrivenKm) {
        this.brand = brand;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.amountOfDrivenKm = amountOfDrivenKm;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public int getAmountOfDrivenKm() {
        return amountOfDrivenKm;
    }

    @Override
    public String toString() {

        return "taskThreeCar.Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", amountOfDrivenKm=" + amountOfDrivenKm +
                '}';

    }

    public static Comparator<Car> getBrandComparator() {
        return Comparator.comparing(Car::getBrand);
    }

    public static Comparator<Car> getModelComparator() {
        return Comparator.comparing(Car::getModel);
    }

    public static Comparator<Car> getYearOfProductionComparator() {
        return Comparator.comparing(Car::getYearOfProduction);
    }

    public static Comparator<Car> getAmountOfDrivenKmComparator() {
        return Comparator.comparing(Car::getAmountOfDrivenKm);
    }

    public static Comparator<Car> getBrandAndModelComparator(){
        return Comparator.comparing(Car::getBrand).thenComparing(Car::getModel);
    }

    @Override
    public int compareTo(Car o) {
        return this.getBrand().compareTo(o.getBrand());
    }
}