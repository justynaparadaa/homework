package aniaCar;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<CarOdKozlo> cars = new ArrayList<>();

        cars.add(new CarOdKozlo("VW", "Polo", 2019, 100));
        cars.add(new CarOdKozlo("VW", "Golf", 2018, 1));
        cars.add(new CarOdKozlo("VW", "Passat", 1999, 200));
        cars.add(new CarOdKozlo("VW", "Arteon", 2010, 30000));
        cars.add(new CarOdKozlo("VW", "Up!", 2000, 1222223));


        cars.sort(CarOdKozlo.getModelComparator());

        for (CarOdKozlo car: cars){
            car.toString();
            System.out.println(car);
        }

    }
}
