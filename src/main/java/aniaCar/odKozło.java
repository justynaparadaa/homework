package aniaCar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class CarOdKozlo implements Comparable<CarOdKozlo> {

    public static void main(String[] args) {

        List<CarOdKozlo> cars = new ArrayList<>();

        cars.add(new CarOdKozlo("VW", "Polo", 2019, 100));
        cars.add(new CarOdKozlo("VW", "Golf", 2018, 1));
        cars.add(new CarOdKozlo("VW", "Passat", 1999, 200));
        cars.add(new CarOdKozlo("VW", "Arteon", 2010, 30000));
        cars.add(new CarOdKozlo("VW", "Up!", 2000, 1222223));


        cars.sort(getModelComparator());
        cars.sort(getVehicleKilometersTraveledComperator());
         for (CarOdKozlo car: cars) {
             cars.toString();
         }

        System.out.println(cars);
    }

    private String brand;
    private String model;
    private int yearOfProduction;
    private int vehicleKilometersTraveled;

    public CarOdKozlo(String brand, String model, int yearOfProduction, int vehicleKilometersTraveled) {
        this.brand = brand;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.vehicleKilometersTraveled = vehicleKilometersTraveled;
    }


    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public int getVehicleKilometersTraveled() {
        return vehicleKilometersTraveled;
    }


    public String toString() {
        return "CarOdKozlo{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", vehicleKilometersTraveled=" + vehicleKilometersTraveled +
                '}';
    }


    public static Comparator<CarOdKozlo> getModelComparator() {
        return Comparator.comparing(CarOdKozlo::getModel);
    }

    public static Comparator<CarOdKozlo> getModelAndBrandComparator() {
        return Comparator.comparing(CarOdKozlo::getBrand).thenComparing(CarOdKozlo::getModel);

    }

    public static Comparator<CarOdKozlo> getYearOfProductionComparator() {
        return Comparator.comparing(CarOdKozlo::getYearOfProduction);
    }

    public static Comparator<CarOdKozlo> getVehicleKilometersTraveledComperator() {
        return Comparator.comparing(CarOdKozlo::getVehicleKilometersTraveled);

    }

    @Override
    public int compareTo(CarOdKozlo o) {
        return this.brand.compareTo(o.getBrand());
    }


}
